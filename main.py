from flask import Flask, redirect, render_template, request
import thread
import glob, os, sys
import webview
import subprocess
import re
import shlex
import time
import commands
from psutil import virtual_memory
# os.chdir(os.path.expanduser("~"))

p = subprocess.Popen(["bash", "-c", "xdpyinfo | grep dimensions"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out,err = p.communicate()
out = out.split()[1]
width = out.split('x')[0]
height = out.split('x')[1]
class PCIDevice:
    address = None
    type = None
    desc = None
    subsystem = None
    flags = None
    caps = None
    driver = None
    def __init__(self, lspci_out=""):
        self.address = re.search('[0-9a-f]{2}:[0-9a-f]{2}\.[0-9a-f]', lspci_out)
        try:
            self.address = self.address.group(0)
        except:
            self.address = None
        self.type = re.search(' (.+?):', lspci_out)
        try:
            self.type = self.type.group(1)
        except:
            self.type = None
        self.desc = re.findall(': (.*?)\n', lspci_out, re.DOTALL)[0]
        self.subsystem = re.search('Subsystem: (.+?)\n', lspci_out)
        try:
            self.subsystem = self.subsystem.group(1)
        except:
            self.subsystem = None
        self.flags = re.search('Flags: (.*?)\n', lspci_out)
        try:
            self.flags = self.flags.group(1)
        except:
            self.flags = None
        self.caps = re.findall('Capabilities: (.*?)\n', lspci_out, re.DOTALL)
        self.driver = re.search('Kernel driver.+?: (.+)', lspci_out)
        try:
            self.driver = self.driver.group(1)
        except:
            self.driver = None

    def __repr__(self):
        return str(self.type) + '\n' + str(self.subsystem) + '\n' + str(self.desc) + '\n' + str(self.address)  + '\n' + str(self.flags) + '\n\n'

class PCI:
    devices = []
    def __init__(self):
        if cmd_exists('lspci'):
            self.devices=[]
            p = subprocess.Popen(['lspci', '-v'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out,err = p.communicate()
            for dev in out.split('\n\n'):
                if dev != "":
                    self.devices.append(PCIDevice(dev))
        else:
            p = subprocess.Popen(['sudo', 'apt-get', 'update', '-y'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out,err = p.communicate()
            p = subprocess.Popen(['sudo', 'apt-get', 'install', '-y', 'pciutils'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out,err = p.communicate()

        

def cmd_exists(cmd):
    return subprocess.call("type " + cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0

def vfio_bind(devices = None):
    # commands.getoutput('xinput disable `xinput | grep -i Mouse | tr -d" " | cut -f2 | cut -d"=" -f2`')
    # time.sleep(1)
    commands.getoutput('unclutter -idle 0.1 -root &')
    for dev in devices:
        device = None
        vendor = None
        with open('/sys/bus/pci/devices/0000:'+dev+'/device', 'r') as fd:
            for line in fd:
                device = line.split('\n')[0]
                break
        with open('/sys/bus/pci/devices/0000:'+dev+'/vendor', 'r') as fd:
            for line in fd:
                vendor = line.split('\n')[0]
                break
        if os.path.isdir('/sys/bus/pci/devices/0000:'+ dev + '/driver'):
            p = subprocess.Popen(['sudo', '/bin/bash', '-c', 'echo 0000:'+ dev +' > /sys/bus/pci/devices/0000:' + dev + '/driver/unbind'], stdout=subprocess.PIPE)
            p.communicate()
        p = subprocess.Popen(['sudo', '/bin/bash', '-c', 'echo ' + vendor + ' '+ device + ' > /sys/bus/pci/drivers/vfio-pci/new_id'], stdout=subprocess.PIPE)
        p.communicate()
     
def vfio_unbind(devices = None, old_devices = None):
    for dev in devices:
        device = None
        vendor = None
        with open('/sys/bus/pci/devices/0000:'+dev+'/device', 'r') as fd:
            for line in fd:
                device = line.split('\n')[0]
                break
        with open('/sys/bus/pci/devices/0000:'+dev+'/vendor', 'r') as fd:
            for line in fd:
                vendor = line.split('\n')[0]
                break
        if os.path.isdir('/sys/bus/pci/devices/0000:'+ dev + '/driver'):
            p = subprocess.Popen(['sudo', '/bin/bash', '-c', 'echo 0000:'+ dev +' > /sys/bus/pci/devices/0000:' + dev + '/driver/unbind'], stdout=subprocess.PIPE)
            p.communicate()
        p = subprocess.Popen(['sudo', '/bin/bash', '-c', 'echo 0000:' + dev + ' > /sys/bus/pci/drivers/' + [ x.driver for x in old_devices.devices if x.address == dev][0] + '/bind'], stdout=subprocess.PIPE)
        # p = subprocess.Popen(['sudo', '/bin/bash', '-c', 'echo ' + vendor + ' '+ device + ' > /sys/bus/pci/drivers/ehci-pci/new_id'], stdout=subprocess.PIPE)
        
        p.communicate()
    # commands.getoutput('xinput enable `xinput | grep -i Mouse | tr -d" " | cut -f2 | cut -d"=" -f2`')
    commands.getoutput('pkill -9 unclutter')
 

def get_kvm_cpusets():
    p = subprocess.Popen(["kvm", "-cpu", "?"], stdout=subprocess.PIPE)
    out,none = p.communicate()
    cpusets = []
    out = out.split('\n\n')[0]
    for line in out.split('\n'):
        try:
            if line != "":
                cpusets.append(dict(arch=line.split()[0], name=line.split()[1], type=' '.join(line.split()[2:])))
        except:
            cpusets.append(dict(arch=line.split()[0], name=line.split()[1], type=line.split()[1]))
    return cpusets

def get_kvm_systemsets():
    p = subprocess.Popen(["kvm", "-machine", "?"], stdout=subprocess.PIPE)
    out,none = p.communicate()
    systemsets = []
    out = out.split(':')[1]
    for line in out.split('\n'):
        try:
            if line != "":
                systemsets.append(dict(name=line.split()[0], type=' '.join(line.split()[1:])))
        except:
            systemsets.append(dict(name=line.split()[0], type=line.split()[0]))
    return systemsets


app = Flask(__name__)
@app.route("/")
def main(error=None):
    # Find available images

    images = []
    for file in os.listdir("."):
        if file.endswith(".qcow2") or file.endswith(".raw"):
            images.append(file)
    if error == 'true':
        error = "An error occured, please try again"
    # Find PCI devices
    pci = PCI()

    # get memory in bytes
    mem = virtual_memory()
    mem = mem.total / (1024 ** 2) - 4000
    return render_template('get.html',error=error, images=images, pci=pci.devices, maxmem=mem,cpusets=get_kvm_cpusets(),systemsets=get_kvm_systemsets())

@app.route("/post", methods=['POST'])
def launch():
    # subprocess.call("pkill -9 python", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0



    # kvm -m <MEGS>
    mem = " -m " + request.form['mem']

    # kvm -cpu <CPUSET>
    cpu = " -cpu " + request.form['cpuset'] + " "

    # kvm -machine <SYSTEMSET>
    machine = " -machine " + request.form['systemset']

    # kvm -f image_type
    image_type = request.form['image'].split('.')[-1]

    cdrom = ""
    if os.path.exists('/dev/cdrom'):
        cdrom = " -cdrom /dev/cdrom "
    
    # kvm device vfio-pci,host=<address>
    passthrough_devs = request.form.getlist('devices')
    pci_devices = " "
    for dev in passthrough_devs:
        pci_devices = pci_devices + ' -device vfio-pci,host=' + dev
    pci = PCI()
    if os.path.isdir("data") == True:
        share = ",smb=$HOME/data"
    elif os.path.isdir("share") == True:
        share = ",smb=$HOME/share"
    else:
        share = ""
    cli = u'kvm -display sdl -full-screen -vga ' + request.form['graphic'] + cdrom + ' -drive file=' + request.form['image'] + ',format=' + image_type + mem + pci_devices + cpu + request.form['net'] +  machine + request.form['cli_args']
    print cli

    err = ""
    try:
        # vfio-bind
        vfio_bind(devices = request.form.getlist('devices'))
        p = subprocess.Popen(shlex.split(cli), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = p.communicate()
    except:
        # vfio-unbind
        vfio_unbind(devices = request.form.getlist('devices'), old_devices=pci)
        return main(error="true")
    # if p.returncode != 0:
    #     return main(error="true")
        
    # vfio-unbind
    vfio_unbind(devices = request.form.getlist('devices'), old_devices=pci)
    return render_template('post.html',cli=cli, err=err, post=request.form)

    # return str(request.form)
    # return str(p.returncode)

@app.route("/poweroff", methods=["GET"])
def poweroff():
    os.system("sudo shutdown now -h")
    return "Hoffentlich hats geklappt"

def flaskThread():
    app.run(host="127.0.0.1")

if __name__ == "__main__":
    p = subprocess.Popen(["sudo", "modprobe", "vfio-pci"])
    p.communicate()
    thread.start_new_thread(flaskThread,())
    window = webview.create_window("Launch VM", "http://localhost:5000", width=int(width), height=int(height), fullscreen=True)

