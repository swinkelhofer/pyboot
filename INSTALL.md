# Installation

## Packages

On linux GTK systems you need to install some additional packages with:
```shell
sudo apt-get install python-gtk2 python-gtk2-dev python-gobject-dev python-gobject  python-webkit python-webkit-dev libwebkit-dev libwebkitgtk-3.0-*
```

## Python Packages

It's only necessary to run pip install in virtualenv
```shell
pip install -r requirements.txt
```
